package com.xedafar.test_app

import android.app.Application
import com.xedafar.test_app.data.di.coinsApiModule
import com.xedafar.test_app.data.di.retrofitModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class TestApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        initKoin()
    }

    private fun initKoin() {
        startKoin {
            androidContext(this@TestApplication)
            modules(
                listOf(
                    retrofitModule,
                    coinsApiModule,
                )
            )
        }
    }
}