package com.xedafar.test_app.base

interface BaseMapper<in A, out B> {
    fun map(type: A): B
}