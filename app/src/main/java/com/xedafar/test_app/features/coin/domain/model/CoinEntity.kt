package com.xedafar.test_app.features.coin.domain.model

data class CoinEntity(
    val id: Int,
    val uid: String,
    val coin_name: String,
    val acronym: String,
    val logo: String
)

