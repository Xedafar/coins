package com.xedafar.test_app.features.coin.domain

import com.xedafar.test_app.features.coin.domain.model.CoinEntity
import io.reactivex.Observable

interface CoinsRepository {
    fun getAllCoins(): Observable<List<CoinEntity>>?
}