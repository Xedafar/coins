package com.xedafar.test_app.features.coin.ui

data class CoinRecyclerUIModel(
    val id: Int,
    val acronym: String,
    val logo: String
)