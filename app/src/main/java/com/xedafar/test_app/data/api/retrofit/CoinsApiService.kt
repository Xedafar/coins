package com.xedafar.test_app.data.api.retrofit

import com.xedafar.test_app.data.api.model.CoinApi
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface CoinsApiService {
    @GET("random_crypto_coin?")
    fun getAllCoins(
        @Query("size") size: String
    ): Observable<List<CoinApi>>
}
