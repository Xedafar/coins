package com.xedafar.test_app.data.di

import com.xedafar.test_app.data.api.retrofit.CoinsApiService
import com.xedafar.test_app.data.reposytory.CoinsRepositoryImpl
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

private const val URL_BASE = "https://random-data-api.com/api/crypto/"

val retrofitModule = module {
    single { provideRetrofitInstance() }
}

private fun provideRetrofitInstance(): Retrofit = Retrofit.Builder()
    .baseUrl(URL_BASE)
    .addConverterFactory(GsonConverterFactory.create())
    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
    .build()

val coinsApiModule = module {
    factory { provideCoinsApiService(retrofit = get()) }
    single {
        CoinsRepositoryImpl(
            coinsNetworkDataSource = get()
        )
    }
}

private fun provideCoinsApiService(retrofit: Retrofit): CoinsApiService =
    retrofit.create(CoinsApiService::class.java)