package com.xedafar.test_app.data.api.model

import com.google.gson.annotations.SerializedName

data class CoinApi(
    @SerializedName("id") val id: Int,
    @SerializedName("uid") val uid: String,
    @SerializedName("coin_name") val coin_name: String,
    @SerializedName("acronym") val acronym: String,
    @SerializedName("logo") val logo: String
)
