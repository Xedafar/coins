package com.xedafar.test_app.data.reposytory

import com.xedafar.test_app.base.BaseMapper
import com.xedafar.test_app.data.api.model.CoinApi
import com.xedafar.test_app.features.coin.domain.model.CoinEntity


object ApiToEntityMapper : BaseMapper<List<CoinApi>, List<CoinEntity>> {
    override fun map(type: List<CoinApi>): List<CoinEntity> {
        return type.map {
            CoinEntity(
                id = it.id,
                uid = it.uid,
                coin_name = it.coin_name,
                acronym = it.acronym,
                logo = it.logo
            )
        }.toList()

    }
}