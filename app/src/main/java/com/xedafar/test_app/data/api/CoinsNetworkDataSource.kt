package com.xedafar.test_app.data.api

import com.xedafar.test_app.data.api.model.CoinApi
import com.xedafar.test_app.data.api.retrofit.CoinsApiService
import io.reactivex.Observable

const val MAX_RESULTS: Int = 80

class CoinsNetworkDataSource(private val coinsApiService: CoinsApiService) {
    fun getAllCoins(size: String = MAX_RESULTS.toString()): Observable<List<CoinApi>> {
        return coinsApiService.getAllCoins(size)
    }
}