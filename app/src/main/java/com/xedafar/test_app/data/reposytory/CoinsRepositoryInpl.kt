package com.xedafar.test_app.data.reposytory

import com.xedafar.test_app.data.api.CoinsNetworkDataSource
import com.xedafar.test_app.features.coin.domain.CoinsRepository
import com.xedafar.test_app.features.coin.domain.model.CoinEntity
import io.reactivex.Observable

class CoinsRepositoryImpl(
    private val coinsNetworkDataSource: CoinsNetworkDataSource
) : CoinsRepository {

    override fun getAllCoins(): Observable<List<CoinEntity>>? {
        var result: Observable<List<CoinEntity>>?
        coinsNetworkDataSource.getAllCoins().let { resultResponse ->
            result = resultResponse.map { ApiToEntityMapper.map(it) }
        }
        return result
    }
}